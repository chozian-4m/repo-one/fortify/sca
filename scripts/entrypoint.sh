#!/bin/bash -e
set -e -o pipefail

if ! [ -f "${FORTIFY_LICENSE}" ] || ! [ -r "${FORTIFY_LICENSE}" ] || ! [ -s "${FORTIFY_LICENSE}" ]; then
    echo "Invalid or missing licence at ${FORTIFY_LICENSE}"
    exit 21
fi

if [ "${FORTIFY_LICENSE}" != "${FORTIFY_HOME}/fortify.license" ]; then
    cat "${FORTIFY_LICENSE}" >"${FORTIFY_HOME}/fortify.license"
fi

mkdir -p "${DATA_HOME}/config/ExternalMetadata"
mkdir -p "${DATA_HOME}/config/rules"

if ! [ -f "${DATA_HOME}/config/installation.properties" ]; then
    echo "guid=$(uuid -v4)" >/tmp/installation.properties
    mv /tmp/installation.properties "${DATA_HOME}/config/installation.properties"
fi

sed_server_properties() {
    sed "$@" <"${FORTIFY_HOME}/Core/config/server.properties" >/tmp/server.properties
    cat /tmp/server.properties >"${FORTIFY_HOME}/Core/config/server.properties"
    rm /tmp/server.properties
}

if [ -n "${FORTIFY_UPDATE_SERVER}" ]; then
    if [[ ${FORTIFY_UPDATE_SERVER} =~ ^[^\#\"\\]+$ ]]; then
        sed_server_properties "/rulepackupdate.server=/s#=.*\$#=${FORTIFY_UPDATE_SERVER//=/\\\\=}#"
    else
        echo "Invalid FORTIFY_UPDATE_SERVER value: ${FORTIFY_UPDATE_SERVER}"
        exit 31
    fi
fi

if [ -n "${FORTIFY_UPDATE_PROXY_HOST}" ]; then
    if [[ ${FORTIFY_UPDATE_PROXY_HOST} =~ ^[-a-zA-Z0-9.]+$ ]]; then
        sed_server_properties "/rulepackupdate.proxy.server=/s#=.*\$#=${FORTIFY_UPDATE_PROXY_HOST}#"
    else
        echo "Invalid FORTIFY_UPDATE_PROXY_HOST value: ${FORTIFY_UPDATE_PROXY_HOST}"
        exit 32
    fi
fi

if [ -n "${FORTIFY_UPDATE_PROXY_PORT}" ]; then
    if [[ ${FORTIFY_UPDATE_PROXY_PORT} =~ ^[0-9]+$ ]]; then
        sed_server_properties "/rulepackupdate.proxy.port=/s#=.*\$#=${FORTIFY_UPDATE_PROXY_PORT}#"
    else
        echo "Invalid FORTIFY_UPDATE_PROXY_PORT value: ${FORTIFY_UPDATE_PROXY_PORT}"
        exit 33
    fi
fi

help_and_exit() {
    if [ $# -gt 0 ]; then
        echo "invalid usage: $*"
    fi
    echo "Available commands:"
    find "$FORTIFY_HOME/bin" -type f -perm /u=x \! -name "autoupdate*.run" -printf '    %f\n'
    exit 1
}

if [ $# -eq 0 ]; then
    help_and_exit "no arguments provided"
fi

case "$1" in
help | -help | --help | -h | '-?')
    help_and_exit
    ;;
esac

exec "$@" || help_and_exit "no such command"
