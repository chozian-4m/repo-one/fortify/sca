# Fortify Static Code Analyzer

## Description

Fortify Static Code Analyzer (SCA) v22.1.0.0151 based on RedHat UBI 8 Minimal.

The container runs sourceanalyzer and other Fortify commandline applications.

SCA functionality is limited to scanning sources of languages not requiring installed frameworks or Microsoft Windows.

Full product documentation is available
[online](https://www.microfocus.com/documentation/fortify-static-code/).

## Licensing

See `LICENSE` file for End User License Agreement.

This is a commercial product. A valid license must be obtained from Micro Focus Fortify before use. Please visit https://www.mfgsinc.com/contact-us for more information about obtaining a license.

## Usage

The container must be provided with:
 * Fortify license file (fortify.license)

The container is configured using environment variables. Some environment variables are set automatically,
if a file at designated location exists. See column "Automatic value" in supported environment variable listing.

The container needs a persistent volume mounted to `/fortify` directory to persist rulepacks, build sessions, and logs.

List available commands:

```sh
$ docker run --rm --name sca -v fortify-sca:/fortify -v "$(pwd)/secrets/fortify.license:/app/sca/fortify.license:ro" \
    "${SCA_IMAGE}" \
    help
```

Fetch SCA rulepacks and store them in fortify-sca volume:

```sh
$ docker run --rm --name sca -v fortify-sca:/fortify -v "$(pwd)/secrets/fortify.license:/app/sca/fortify.license:ro" \
    "${SCA_IMAGE}" \
    fortifyupdate
```

Scan source code in `$(pwd)/sources`:

```sh
$ docker run --rm --name sca -v fortify-sca:/fortify -v "$(pwd)/secrets/fortify.license:/app/sca/fortify.license:ro" \
    -v "$(pwd)/sources:/sources:ro" \
    "${SCA_IMAGE}" \
    sourceanalyzer "/sources/**/*.java" -scan
```

Scan source code in `$(pwd)/sources` with memory limit:

```sh
$ docker run --rm --name sca -v fortify-sca:/fortify -v "$(pwd)/secrets/fortify.license:/app/sca/fortify.license:ro" \
    -v "$(pwd)/sources:/sources:ro" \
    --memory=8g
    "${SCA_IMAGE}" \
    sourceanalyzer -Xmx6g "/sources/**/*.java" -scan
```

### Environment Variables

Variable | Description | Default value | Required
--- | --- | --- | ---
FORTIFY_LICENSE | Path to Fortify license file. | `/app/sca/fortify.license` | Yes
FORTIFY_UPDATE_SERVER | Fortify update server URL. | `https://update.fortify.com` | No
FORTIFY_UPDATE_PROXY_HOST | HTTP proxy host for Fortify update server. | | No
FORTIFY_UPDATE_PROXY_PORT | HTTP proxy port for Fortify update server. | | No

### Resource Requirements

Refer to the online documentation for details.

Note: the sourceanalyzer application does not understand container memory limits. When container memory limit is set,
maximum Java heap size must be provided. See examples for how to do it.

Resource | Minimal | Recommended
--- | --- | ---
CPU | 1 core | more cores will run scans faster
RAM | 8 GiB | 16 GiB (32 GiB for scanning dynamic languages) or more for complex projects and faster scans
Persistent storage | 1 GiB | 8 GiB

The application is using a single volume. The volume should be unshared and persistent.
