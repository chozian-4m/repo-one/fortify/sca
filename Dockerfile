###
# Base image specification build arguments
ARG BASE_REGISTRY="registry1.dso.mil"
ARG BASE_IMAGE="ironbank/redhat/ubi/ubi8-minimal"
ARG BASE_TAG="8.5"

###
# Build image stage
FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG} as build
ARG DNF="microdnf"
ARG UID="1111"

# Common build environment
ENV APP_HOME="/app"

ARG DOWNLOAD_DIR="."

# Install and prepare SCA
ARG SCA_VERSION="22.1.0.0151"
ENV FORTIFY_HOME="${APP_HOME}/sca"
ARG SCA_TARBALL="Fortify_SCA_${SCA_VERSION}.tar.gz"

# Prepare and unpack tarball
RUN "${DNF}" install -y --nodocs tar gzip \
     && "${DNF}" -y clean all

COPY ${DOWNLOAD_DIR}/${SCA_TARBALL} /tmp
RUN mkdir -p ${FORTIFY_HOME} \
&& tar -zxf /tmp/${SCA_TARBALL} -C ${FORTIFY_HOME}

RUN exec > /tmp/fortify.license \
    && cp /tmp/fortify.license "${FORTIFY_HOME}/fortify.license" \
    && ( cd "${FORTIFY_HOME}/bin" && rm scapostinstall ReportGenerator ) \
    && rm -r "${FORTIFY_HOME}/plugins" \
    && chgrp -R +0 "${FORTIFY_HOME}" \
    && chmod -R go-w "${FORTIFY_HOME}" \
    && mv "${FORTIFY_HOME}/Core/config/ExternalMetadata" "${FORTIFY_HOME}/Core/config/ExternalMetadata-orig" \
    && mv "${FORTIFY_HOME}/Core/config/rules" "${FORTIFY_HOME}/Core/config/rules-orig" \
    && :

###
# Runtime image stage
FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}
ARG DNF="microdnf"
ARG UID="1111"

RUN "${DNF}" install -y --nodocs findutils uuid && "${DNF}" -y clean all

# Common runtime environment
ENV APP_HOME="/app"
ENV DATA_HOME="/fortify"

# Copy prepared SCA
ENV FORTIFY_HOME="${APP_HOME}/sca"
ENV FORTIFY_LICENSE="${FORTIFY_HOME}/fortify.license"
COPY --from=build "${FORTIFY_HOME}" "${FORTIFY_HOME}"

# Customize SCA configuration
RUN chown "${UID}" "${FORTIFY_LICENSE}" && chmod 660 "${FORTIFY_LICENSE}"
RUN f="${FORTIFY_HOME}/Core/config/server.properties" && chown "${UID}" "${f}" && chmod 660 "${f}"
RUN cd "${FORTIFY_HOME}/Core/config" \
    && ln -s "${DATA_HOME}/config/installation.properties" \
    && ln -s "${DATA_HOME}/config/ExternalMetadata" \
    && ln -s "${DATA_HOME}/config/rules" \
    && :

# Put SCA on PATH
ENV PATH="${PATH}:${FORTIFY_HOME}/bin"

# Copy common files
COPY README.md LICENSE /

# Provide and set custom entrypoint
COPY "scripts/entrypoint.sh" "/entrypoint.sh"
ENTRYPOINT [ "/bin/bash", "--", "/entrypoint.sh" ]

# Switch to fortify volume and make it writable by container user
WORKDIR "${DATA_HOME}"
RUN chown "${UID}" "${DATA_HOME}" && chmod 770 "${DATA_HOME}"

# Run container with as non-root user
ENV HOME="${DATA_HOME}"
ENV USER="fortify"
ENV LOGNAME="fortify"
RUN echo "${USER}:x:${UID}:0:Fortify User:${HOME}:/bin/bash" >> /etc/passwd
USER "${UID}"

# Declare fortify volume containing all container state
VOLUME [ "${DATA_HOME}" ]

ARG SCA_VERSION="22.1.0.0151"
ENV COM_FORTIFY_VERSION_SCA="${SCA_VERSION}"

ENV COM_FORTIFY_VERSION_CONTAINER="22.1.0.0151.0"
